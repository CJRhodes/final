﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerController : MonoBehaviour
{
    public float moveForce = 5, jumpMulitplier = 2, jHeight, speed;
    public bool inAir;

    Rigidbody2D myBody;

    void Start()
    {
        myBody = this.GetComponent<Rigidbody2D>();

    }


    void FixedUpdate()
    {

        Vector2 moveVec = new Vector2(CrossPlatformInputManager.GetAxis("Horizontal"), CrossPlatformInputManager.GetAxis("Vertical")) * moveForce;
        bool isJumping = CrossPlatformInputManager.GetButton("Jump");
        Debug.Log(isJumping ? jumpMulitplier : 1);
        myBody.AddForce(moveVec * (isJumping ? jumpMulitplier : 1));
        if (!inAir && (CrossPlatformInputManager.GetButton("Jump")))
        {
            myBody.AddForce(new Vector2(myBody.velocity.x, jHeight));
            inAir = true;
        }
        if (CrossPlatformInputManager.GetAxis("Horizontal") <= .5 && CrossPlatformInputManager.GetAxis("Horizontal") > 0)
        {
            myBody.position += Vector2.right * speed * Time.deltaTime;
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        inAir = false;
    }
}
